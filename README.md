# README #

- The goals are to collect coins and keys, upon collecting a key for a given area the player must return to the central hub,
- and access the newly unlocked area. Upon collecting all keys and returning to the start area the player can access the 
- puzzle mechanism minigame. When the minigame is finished the player is taken to a new camera and lightness consumes the game.
- This signifies the games end and the player winning.

- There are multiple different types of darkness (enemy) movement.

- While the character and darkness are touching, the player loses heatlh.

- The player starts with a basic light that flies slowly from the player in the direction the player is facing.
- This light can be used an unlimited amount of times, but is limited to a maximum number of shots per second. 
- The player has a variety of different light 'weapons' that can be picked up throughout the game
- Other lights include, but are not limited to, “shield / barrier” that will force a darkness to run away when it touches the barrier.

- Endgame puzzle mechanism in which the player controls a sphere rolling on a 2D sidescroller-esque platform.

- The game allows the player to pause the gameplay, save, and load.

