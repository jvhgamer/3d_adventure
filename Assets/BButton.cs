﻿using UnityEngine;
using System.Collections;

public class BButton : MonoBehaviour {

	public GameObject bdoor;
	public GameObject stationarygreen;

	void Start ()
	{
		stationarygreen.gameObject.SetActive (false);
	}

	void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.CompareTag ("Box")) {
			//	AudioClip.(pickUp);
			bdoor.gameObject.SetActive (false);
			global.bdoor = true;
			other.gameObject.SetActive(false);
			stationarygreen.gameObject.SetActive (true);

		}
	}
}
