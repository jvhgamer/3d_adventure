﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HealthBar : MonoBehaviour {

	public static float health = 1.0f;
	public GameObject player;
	public Slider healthBar;
	private Vector3 plLoc = new Vector3(194,3,241);
	//public AudioClip Hit;
	public AudioSource audio;

	// Use this for initialization
	void Start () {
		//audio = GetComponent<AudioSource>();

	}
	public void ReduceHealth ()
	{
		health = health - .15f;
		healthBar.value = health;


		audio.Play();

		if (health < .01f) {
			player.transform.position = plLoc;
			health = 1.0f;
			healthBar.value = health;
		}
	}
	// Update is called once per frame
	void Update () {
	
	}
}
