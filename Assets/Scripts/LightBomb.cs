﻿using UnityEngine;
using System.Collections;

public class LightBomb : MonoBehaviour {
	public Light light;
	public Light light2;
	public Light light3;
	public Light light4;

	public Rigidbody rb;
	public float thrust;
	public float timeOn = 0.1f;
	public float timeOff = 0.1f;
	public float changeTime = 0f;
	public float waitTime = 3f;
	public bool boom;

	// Use this for initialization
	void Start () {
		boom = false;
		rb = GetComponent<Rigidbody> ();
		light = GetComponent<Light> ();
		light2 = GetComponent<Light> ();
		light3 = GetComponent<Light> ();
		light3.enabled = false;
		waitTime = Time.deltaTime + waitTime;
	}

	void Update() {

		if (Time.deltaTime > waitTime) {
			if (Time.time > changeTime) {
				light.enabled = !light.enabled;
				if (light.enabled) {
					changeTime = Time.time + timeOn;
				} else {
					changeTime = Time.time + timeOff;
				}
			}
			flash ();
		}
	}
	// Update is called once per frame
	void FixedUpdate () {
		if (boom) {
			light4.enabled = !light4.enabled;
			if (light4.enabled) {
				changeTime = Time.time + timeOn;
			} else {
				changeTime = Time.time + timeOff;
			}
		}
	}

	IEnumerator flash(){
		light.intensity = 8f;
		light.range = 30;
		light2.intensity = 8f;
		light2.range = 30;
		light3.enabled = true;
		light4.enabled = true;
		yield return new WaitForSecondsRealtime(3);
		light4.bounceIntensity = 8f;
		yield return new WaitForSecondsRealtime(5);
		light4.enabled = false;
		yield return new WaitForSecondsRealtime(1);
		light4.enabled = true;
		yield return new WaitForSecondsRealtime(3);
		light4.enabled = false;
		yield return new WaitForSecondsRealtime(1);
		light4.enabled = true;
		boom = true;

	}
}