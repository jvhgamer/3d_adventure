using System;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;
using UnityStandardAssets.Utility;
using Random = UnityEngine.Random;


namespace UnityStandardAssets.Characters.FirstPerson
{
	[RequireComponent(typeof (CharacterController))]
	[RequireComponent(typeof (AudioSource))]
	public class FirstPersonController : MonoBehaviour
	{
		[SerializeField] private bool m_IsWalking;
		[SerializeField] private float m_WalkSpeed;
		[SerializeField] private float m_RunSpeed;
		[SerializeField] [Range(0f, 1f)] private float m_RunstepLenghten;
		[SerializeField] private float m_JumpSpeed;
		[SerializeField] private float m_StickToGroundForce;
		[SerializeField] private float m_GravityMultiplier;
		[SerializeField] private MouseLook m_MouseLook;
		[SerializeField] private bool m_UseFovKick;
		[SerializeField] private FOVKick m_FovKick = new FOVKick();
		[SerializeField] private bool m_UseHeadBob;
		[SerializeField] private CurveControlledBob m_HeadBob = new CurveControlledBob();
		[SerializeField] private LerpControlledBob m_JumpBob = new LerpControlledBob();
		[SerializeField] private float m_StepInterval;
		[SerializeField] private AudioClip[] m_FootstepSounds;    // an array of footstep sounds that will be randomly selected from.
		[SerializeField] private AudioClip m_JumpSound;           // the sound played when character leaves the ground.
		[SerializeField] private AudioClip m_LandSound;           // the sound played when character touches back on ground.
		[SerializeField] private AudioClip m_FlySound;
		[SerializeField] private AudioClip m_FlySound2;
		[SerializeField] public AudioClip m_switchSound;


		private Camera m_Camera;
		private bool m_Jump;
		private float m_YRotation;
		private Vector2 m_Input;
		private Vector3 m_MoveDir = Vector3.zero;
		private CharacterController m_CharacterController;
		private CollisionFlags m_CollisionFlags;
		private bool m_PreviouslyGrounded;
		private Vector3 m_OriginalCameraPosition;
		private float m_StepCycle;
		private float m_NextStep;
		private bool m_Jumping;
		private AudioSource m_AudioSource;
		private bool m_Boost;
		private bool m_BoostDone;
		private bool m_Boosted;
		private bool m_Fly;
		private bool m_FlyDone;
		//private AudioSource m_switchLghtAS;


		// /* addin
		public Rigidbody m_LightShot;                   // Prefab of the shell.
		public Rigidbody m_LightShot1;                   // Prefab of the shell.
		public Rigidbody m_LightShot2;                   // Prefab of the shell.
		public Rigidbody m_LightShot3;                   // Prefab of the shell.


		public Transform m_FireTransform;           // A child of the tank where the shells are spawned.
		public Transform m_FireTransform2;
		private float m_CurrentLaunchForce = 20f;         // The force that will be given to the shell when the fire button is released.
		private float m_ChargeSpeed = 20f;                // How fast the launch force increases, based on the max charge time.
		private bool m_Fired;                       // Whether or not the shell has been launched with this button press.
		private bool m_Fired2;                       // Whether or not the shell has been launched with this button press.


		public float lastFired = 0f;
		public float lastFired2 = 0f;
		public int seeLight_Amo;                    // amo, starts off with 100 shots 
		public AudioSource outOfAmo;
		public AudioSource FlySound;
		public float fireRate;
		public float fireRate2;
		private float nextFire;
		public static int energy;
		public static int keys;
		public float time;
		public float flytime;
		public static int shotSelect; 
		public static int shotSelect2; 
		public float detTime = 0f;
		public float thrust;
		public float timeQ;
		public float timeE;
		// */


		// Use this for initialization
		private void Start()
		{
			shotSelect = 0;
			shotSelect2 = 0;
			m_CharacterController = GetComponent<CharacterController>();
			m_Camera = Camera.main;
			m_OriginalCameraPosition = m_Camera.transform.localPosition;
			m_FovKick.Setup(m_Camera);
			m_HeadBob.Setup(m_Camera, m_StepInterval);
			m_StepCycle = 0f;
			m_NextStep = m_StepCycle/2f;
			m_Jumping = false;
			m_AudioSource = GetComponent<AudioSource>();
			m_MouseLook.Init(transform , m_Camera.transform);
			m_Boost= false;
			m_BoostDone = true;
			m_Fly = false;
			m_FlyDone = true;
			m_Boosted = false;
			keys = 0;
			energy = 10;
			time = 5.0f;
			timeQ = 0.0f;
			timeE = 0.0f;
			flytime = 6.0f;
		}
		// Update is called once per frame
		private void Update()
		{

			seeLight_Amo = global.Light_Amo;
			RotateView();
			// the jump state needs to read here to make sure it is not missed
			if (!m_Jump)
			{
				m_Jump = CrossPlatformInputManager.GetButtonDown("Jump");
			}


			if (!m_PreviouslyGrounded && m_CharacterController.isGrounded)
			{
				StartCoroutine(m_JumpBob.DoBobCycle());
				PlayLandingSound();
				m_MoveDir.y = 0f;
				m_Jumping = false;
			}
			if (!m_CharacterController.isGrounded && !m_Jumping && m_PreviouslyGrounded)
			{
				m_MoveDir.y = 0f;
			}
			if (energy > 3 && !m_Boost && m_BoostDone) {
				m_Boost = Input.GetKeyDown (KeyCode.RightShift);
				if (m_Boost){
					m_BoostDone = false;
					energy = energy-3;
				}
			}
			if (energy > 4 && !m_Fly && m_FlyDone) {
				m_Fly = Input.GetKeyDown (KeyCode.Return);
				if (m_Fly){
					m_FlyDone = false;
					energy = energy-4;
				}
			}
			m_PreviouslyGrounded = m_CharacterController.isGrounded;

		
			if (Input.GetButtonDown("Fire1") ){ // left click = 0 fire
				Fire ();
			}//end mouse click 0 (left mouse click)
			/* switch shot key*/
			if (Input.GetButtonDown("Fire2")) { // right click = 0  fire2
				Fire2 ();
			}
			// Q
			/* switch shot key*/
			if (Input.GetKey(KeyCode.Q) && Time.timeSinceLevelLoad > timeQ) { // q click = 0  (switch weapon)
				timeQ = Time.timeSinceLevelLoad + 1;
				if (shotSelect == 0) {       
					shotSelect = 1;
				}else if (shotSelect == 1) {       
					shotSelect = 2;
				}else if (shotSelect == 2) {       
					shotSelect = 3;
				}else if (shotSelect == 3) {       
					shotSelect = 0;
				}
				m_AudioSource.clip = m_switchSound;
				m_AudioSource.Play ();
			}
			// E
			/* switch shot key*/
			if (Input.GetKey(KeyCode.E) && Time.timeSinceLevelLoad > timeE) { // e click = 0  (switch weapon)
				timeE = Time.timeSinceLevelLoad + 1;
				if (shotSelect2 == 0) {       
					shotSelect2 = 1;
				}else if (shotSelect2 == 1) {       
					shotSelect2 = 2;
				}else if (shotSelect2 == 2) {       
					shotSelect2 = 3;
				}else if (shotSelect2 == 3) {       
					shotSelect2 = 0;
				}
				m_AudioSource.clip = m_switchSound;
				m_AudioSource.Play ();
			}
		}// end update
			



		private void PlayLandingSound()
		{
			m_AudioSource.clip = m_LandSound;
			m_AudioSource.Play();
			m_NextStep = m_StepCycle + .5f;
		}
		private void PlayFlySound()
		{
			m_AudioSource.clip = m_FlySound;
			m_AudioSource.Play();
		}
		private void PlayFlySound2()
		{
			m_AudioSource.Stop();
			m_AudioSource.clip = m_FlySound2;
			m_AudioSource.Play();


		}


		private void FixedUpdate()
		{
			float speed;
			GetInput(out speed);
			if (m_Boost && time > 0.0f && !m_Boosted) {
				m_RunSpeed = m_RunSpeed + 5;
				m_WalkSpeed = m_WalkSpeed + 5;
				time -= Time.deltaTime;
				m_Boosted = true;
			} else if (m_Boost && time > 0.0f) {
				time -= Time.deltaTime;
			} else if (time < 0.0f) {
				m_Boost = false;
				m_BoostDone = true;
				m_Boosted = false;
				time = 5.0f;
				m_RunSpeed = m_RunSpeed - 5;
				m_WalkSpeed = m_WalkSpeed - 5;
			} 
			else {
			}
			// always move along the camera forward as it is the direction that it being aimed at
			Vector3 desiredMove = transform.forward*m_Input.y + transform.right*m_Input.x;


			// get a normal for the surface that is being touched to move along it
			RaycastHit hitInfo;
			Physics.SphereCast(transform.position, m_CharacterController.radius, Vector3.down, out hitInfo,
				m_CharacterController.height/2f, ~0, QueryTriggerInteraction.Ignore);
			desiredMove = Vector3.ProjectOnPlane(desiredMove, hitInfo.normal).normalized;


			m_MoveDir.x = desiredMove.x*speed;
			m_MoveDir.z = desiredMove.z*speed;


			if (m_Fly && flytime == 6) {
				PlayFlySound ();
			}
			if (m_Fly && flytime >= 4.75f) {
				m_GravityMultiplier = 0;
				m_MoveDir.y = m_JumpSpeed - 4;
				flytime -= Time.deltaTime;
			} else if (m_Fly && flytime>=4.60) {
				PlayFlySound2 ();
				flytime -= Time.deltaTime;
			}
			else if (m_Fly && flytime > 0.0f) {
				m_GravityMultiplier = 0;
				flytime -= Time.deltaTime;
				m_MoveDir.y = 0;
			} 
			else if(m_Fly && flytime < 0.0f) {
				m_Fly = false;
				flytime = 6.0f;
				m_FlyDone = true;
				m_GravityMultiplier = 2;
			}
			if (m_Fly) {
				gameObject.GetComponent<ParticleSystem>().enableEmission = true;

			} else {
				gameObject.GetComponent<ParticleSystem>().enableEmission = false;
			}


			if (m_CharacterController.isGrounded && !m_Fly) {
				m_MoveDir.y = -m_StickToGroundForce;


				if (m_Jump) {
					m_MoveDir.y = m_JumpSpeed;
					PlayJumpSound ();
					m_Jump = false;
					m_Jumping = true;
				}
			} else if (m_Jumping && m_Jump) {
				m_MoveDir.y = m_JumpSpeed - 2;
				PlayJumpSound ();
				m_Jumping = false;
				m_Jump = false;
			} else if (!m_Fly) {
				m_MoveDir += Physics.gravity * m_GravityMultiplier * Time.fixedDeltaTime;
			} 
			else {
			}
			m_CollisionFlags = m_CharacterController.Move(m_MoveDir*Time.fixedDeltaTime);


			ProgressStepCycle(speed);
			UpdateCameraPosition(speed);


			m_MouseLook.UpdateCursorLock();
		}

		// /* added fire
		private void Fire (){
			if (Time.time > fireRate + lastFired) {

				switch (shotSelect) {
				case(0):
					m_CurrentLaunchForce = 15f;
					m_Fired = true;
					//global.Light_Amo--;
					Rigidbody light_Instance =
						Instantiate (m_LightShot, m_FireTransform.position, m_FireTransform.rotation) as Rigidbody;
					light_Instance.velocity = m_CurrentLaunchForce * m_FireTransform.forward;
					lastFired = Time.time;

					break;
				case(1):
					m_CurrentLaunchForce = 20f;
					m_Fired = true;
					if (global.Light_Amo1 > 0) {
						global.Light_Amo1--;
						Rigidbody light_Instance1 =
							Instantiate (m_LightShot1, m_FireTransform.position, m_FireTransform.rotation) as Rigidbody;
						light_Instance1.velocity = m_CurrentLaunchForce * m_FireTransform.forward;
						lastFired = Time.time;
					} else { // out of amo
						outOfAmo.Play ();
					}
					break;
				case(2):/* bolt light (lightshot3)*/
					m_CurrentLaunchForce = 75f;
					m_Fired = true;
					if (global.Light_Amo2 > 0) {
						global.Light_Amo2--;
						Rigidbody light_Instance2 =
							Instantiate (m_LightShot2, m_FireTransform.position, m_LightShot2.rotation) as Rigidbody;
						light_Instance2.velocity = m_CurrentLaunchForce * m_FireTransform.forward;
						lastFired = Time.time;
					} else { // out of amo
						outOfAmo.Play ();
					}
					break;
				case(3):/*lightbomb*/
					m_CurrentLaunchForce = 5f;

					if (global.Light_Amo3 > 0) {
						global.Light_Amo3--;
						Rigidbody light_Instance3 =
							Instantiate (m_LightShot3, m_FireTransform2.position, m_FireTransform2.rotation) as Rigidbody;
						light_Instance3.velocity = m_CurrentLaunchForce * m_FireTransform2.forward;
						lastFired = Time.time;
					} else { // out of amo
						outOfAmo.Play ();
					}
					break;
				default:
					/*
				m_Fired = true;
				global.Light_Amo--;
				Rigidbody light_Instance0 =
					Instantiate (m_LightShot, m_FireTransform.position, m_FireTransform.rotation) as Rigidbody;
				light_Instance0.velocity = m_CurrentLaunchForce * m_FireTransform.forward;
				*/
					break;
				}
			}
		}

		// /* added fire2
		private void Fire2 (){
			if (Time.time > fireRate2 + lastFired2) {

				switch (shotSelect2) {
				case(0):
					m_CurrentLaunchForce = 15f;
					m_Fired2 = true;
					//global.Light_Amo--;
					Rigidbody light_Instance4 =
						Instantiate (m_LightShot, m_FireTransform2.position, m_FireTransform2.rotation) as Rigidbody;
					light_Instance4.velocity = m_CurrentLaunchForce * m_FireTransform2.forward;
					lastFired2 = Time.time;

					break;
				case(1):
					m_CurrentLaunchForce = 20f;
					m_Fired2 = true;
					if (global.Light_Amo1 > 0) {
						global.Light_Amo1--;
						Rigidbody light_Instance5 =
							Instantiate (m_LightShot1, m_FireTransform2.position, m_FireTransform2.rotation) as Rigidbody;
						light_Instance5.velocity = m_CurrentLaunchForce * m_FireTransform2.forward;
						lastFired2 = Time.time;
					} else { // out of amo
						outOfAmo.Play ();
					}
					break;
				case(2):/* bolt light (lightshot3)*/
					m_CurrentLaunchForce = 75f;
					m_Fired2 = true;
					if (global.Light_Amo2 > 0) {
						global.Light_Amo2--;
						Rigidbody light_Instance6 =
							Instantiate (m_LightShot2, m_FireTransform2.position, m_LightShot2.rotation) as Rigidbody;
						light_Instance6.velocity = m_CurrentLaunchForce * m_FireTransform2.forward;
						lastFired2 = Time.time;
					} else { // out of amo
						outOfAmo.Play ();
					}
					break;
				case(3):/*lightbomb*/
					m_CurrentLaunchForce = 5f;

					if (global.Light_Amo3 > 0) {
						global.Light_Amo3--;
						Rigidbody light_Instance7 =
							Instantiate (m_LightShot3, m_FireTransform2.position, m_FireTransform2.rotation) as Rigidbody;
						light_Instance7.velocity = m_CurrentLaunchForce * m_FireTransform2.forward;
						lastFired2 = Time.time;
					} else { // out of amo
						outOfAmo.Play ();
					}
					break;
				default:
					/*
				m_Fired = true;
				global.Light_Amo--;
				Rigidbody light_Instance0 =
					Instantiate (m_LightShot, m_FireTransform.position, m_FireTransform.rotation) as Rigidbody;
				light_Instance0.velocity = m_CurrentLaunchForce * m_FireTransform.forward;
				*/
					break;
				}
			}
		}


		private void PlayJumpSound()
		{
			m_AudioSource.clip = m_JumpSound;
			m_AudioSource.Play();
		}




		private void ProgressStepCycle(float speed)
		{
			if (m_CharacterController.velocity.sqrMagnitude > 0 && (m_Input.x != 0 || m_Input.y != 0))
			{
				m_StepCycle += (m_CharacterController.velocity.magnitude + (speed*(m_IsWalking ? 1f : m_RunstepLenghten)))*
					Time.fixedDeltaTime;
			}


			if (!(m_StepCycle > m_NextStep))
			{
				return;
			}


			m_NextStep = m_StepCycle + m_StepInterval;


			PlayFootStepAudio();
		}




		private void PlayFootStepAudio()
		{
			if (!m_CharacterController.isGrounded)
			{
				return;
			}
			// pick & play a random footstep sound from the array,
			// excluding sound at index 0
			int n = Random.Range(1, m_FootstepSounds.Length);
			m_AudioSource.clip = m_FootstepSounds[n];
			m_AudioSource.PlayOneShot(m_AudioSource.clip);
			// move picked sound to index 0 so it's not picked next time
			m_FootstepSounds[n] = m_FootstepSounds[0];
			m_FootstepSounds[0] = m_AudioSource.clip;
		}




		private void UpdateCameraPosition(float speed)
		{
			Vector3 newCameraPosition;
			if (!m_UseHeadBob)
			{
				return;
			}
			if (m_CharacterController.velocity.magnitude > 0 && m_CharacterController.isGrounded)
			{
				m_Camera.transform.localPosition =
					m_HeadBob.DoHeadBob(m_CharacterController.velocity.magnitude +
						(speed*(m_IsWalking ? 1f : m_RunstepLenghten)));
				newCameraPosition = m_Camera.transform.localPosition;
				newCameraPosition.y = m_Camera.transform.localPosition.y - m_JumpBob.Offset();
			}
			else
			{
				newCameraPosition = m_Camera.transform.localPosition;
				newCameraPosition.y = m_OriginalCameraPosition.y - m_JumpBob.Offset();
			}
			m_Camera.transform.localPosition = newCameraPosition;
		}




		private void GetInput(out float speed)
		{
			// Read input
			float horizontal = CrossPlatformInputManager.GetAxis("Horizontal");
			float vertical = CrossPlatformInputManager.GetAxis("Vertical");


			bool waswalking = m_IsWalking;


			#if !MOBILE_INPUT
			// On standalone builds, walk/run speed is modified by a key press.
			// keep track of whether or not the character is walking or running
			m_IsWalking = !Input.GetKey(KeyCode.LeftShift);
			#endif
			// set the desired speed to be walking or running
			speed = m_IsWalking ? m_WalkSpeed : m_RunSpeed;
			m_Input = new Vector2(horizontal, vertical);


			// normalize input if it exceeds 1 in combined length:
			if (m_Input.sqrMagnitude > 1)
			{
				m_Input.Normalize();
			}


			// handle speed change to give an fov kick
			// only if the player is going to a run, is running and the fovkick is to be used
			if (m_IsWalking != waswalking && m_UseFovKick && m_CharacterController.velocity.sqrMagnitude > 0)
			{
				StopAllCoroutines();
				StartCoroutine(!m_IsWalking ? m_FovKick.FOVKickUp() : m_FovKick.FOVKickDown());
			}
		}




		private void RotateView()
		{
			if (Time.timeScale != 0) {
				m_MouseLook.LookRotation (transform, m_Camera.transform);
				m_MouseLook.LookRotation (transform, m_FireTransform.transform);
				m_MouseLook.LookRotation (transform, m_FireTransform2.transform);

			}
		}




		private void OnControllerColliderHit(ControllerColliderHit hit)
		{
			Rigidbody body = hit.collider.attachedRigidbody;
			//dont move the rigidbody if the character is on top of it
			if (m_CollisionFlags == CollisionFlags.Below)
			{
				return;
			}


			if (body == null || body.isKinematic)
			{
				return;
			}
			body.AddForceAtPosition(m_CharacterController.velocity*0.1f, hit.point, ForceMode.Impulse);
		}
	}
}


