﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class shieldPickUp : MonoBehaviour {
	public Rigidbody rb;
	public GameObject go;
	public static bool shieldOn;
	public float time;

	// Use this for initialization
	void Start () 
	{	
		shieldOn = false;
		go.SetActive (false);
	}

	// Update is called once per frame
	void Update () {
		if (shieldOn && time > Time.deltaTime) {
			
		}
		if (shieldOn && time < Time.deltaTime) {
			shieldOn = false;
			go.SetActive (false);
		}
	}

	void OnTriggerEnter(Collider other)
	{

		if (other.gameObject.CompareTag ("shield")){
			other.gameObject.SetActive (false);
			go.SetActive (true);
			shieldOn = true;
			time = Time.deltaTime + 30f;
		}

	}

}