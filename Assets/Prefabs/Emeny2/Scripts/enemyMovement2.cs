﻿using UnityEngine;
using System.Collections;

public class enemyMovement2 : MonoBehaviour {

	public Transform player2;
	public float thrust2 = 0.1f;
	public float Timer2;
	public float TimerMove2;
	public float TimerRun2;

	private Vector3 startLoc2;
	private Quaternion startRot2;

	private bool OutofArea2 = false;
	private bool Free2 = false;
	private bool Run2 = false;

	void Start(){
		startLoc2 = this.transform.position;
		Vector3 direction2 = new Vector3(0, Random.Range(0,360), 0);
		this.transform.rotation = Quaternion.Euler (direction2);
		Timer2 = Time.timeSinceLevelLoad + 25.0f;
		TimerMove2 = Time.timeSinceLevelLoad + 5.0f;
		TimerRun2 = Time.timeSinceLevelLoad;
	}

	void Update () {
		if (Time.timeScale != 0) {
			this.transform.position += (this.transform.forward * thrust2);
			Vector3 direction2 = player2.position - this.transform.position;
			if (Vector3.Distance (player2.position, this.transform.position) < 75 && !OutofArea2 && !Run2) {
				Free2 = false;
				direction2.y = 0;

				this.transform.rotation = Quaternion.Lerp (this.transform.rotation, Quaternion.LookRotation (direction2), 0.1f);

				if (direction2.magnitude > 30) {
					thrust2 = 0.1f;
				} else if (direction2.magnitude > 20) {
					thrust2 = 0.4f;
				} else if (direction2.magnitude > 0) {
					thrust2 = 0.7f;
				}
			}
			Vector3 direction4 = startLoc2 - this.transform.position;
			if (Vector3.Distance (startLoc2, this.transform.position) > 75 && Run2 == false) {
				Free2 = false;
				OutofArea2 = true;
				direction4.y = 0;
				this.transform.rotation = Quaternion.Euler (direction4);
			}
			if (Vector3.Distance (startLoc2, this.transform.position) < 10 || Vector3.Distance (player2.position, this.transform.position) < 15 && Run2 == false) {
				OutofArea2 = false;
			}
			if (Vector3.Distance (startLoc2, this.transform.position) <= 75 && Vector3.Distance (player2.position, this.transform.position) > 75 && Run2 == false) {
				Free2 = true;
			}
			if (Free2 && !Run2) {
				if (Time.timeSinceLevelLoad > Timer2) {
					Timer2 += 25.0f;
					Vector3 direction7 = new Vector3 (0, Random.Range (0, 360), 0);
					this.transform.rotation = Quaternion.Euler (direction7);
				}
			}
			if (Time.timeSinceLevelLoad > TimerRun2 && Run2) {
				Run2 = false;
				Vector3 direction7 = new Vector3 (0, Random.Range (0, 360), 0);
				this.transform.rotation = Quaternion.Euler (direction7);
			}
		}
	}
	void OnTriggerEnter(Collider other){
		if (other.transform.tag == "light") {
			Run2 = true;
			TimerRun2 = Time.timeSinceLevelLoad + 10;
			this.transform.rotation = player2.transform.rotation;
			Destroy (other.gameObject);
			thrust2 = 1f;
		}
	}
}
