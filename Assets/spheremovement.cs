﻿using UnityEngine;
using System.Collections;

public class spheremovement : MonoBehaviour {


	public Rigidbody player;
	public float gravmult = 1750;
	public float fallspeed;

	public float Timer = 0.0f;

	// Use this for initialization
	void Start () {
		Time.timeScale = 10;
		gravmult = 1750;
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey(KeyCode.R))
			Application.LoadLevel (2);
		if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow))
			player.AddForce (35 * player.transform.forward);
		if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
			player.AddForce (35 * player.transform.forward * -1);
		if (Input.GetKey (KeyCode.Space) && Time.timeSinceLevelLoad > Timer) {
			Timer = 5 + Time.timeSinceLevelLoad;
			player.AddForce (gravmult * player.transform.up);
		}
	}
}
